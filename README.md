

![logo][logo]





 
# terraform-backend 


 
Terraform workspace that provision an S3 bucket to store the `terraform.tfstate` file and a DynamoDB table to lock the state file to prevent concurrent modifications and state corruption. 


---


## Screenshots


![s3-bucket](docs/screenshots/1.png) |
|:--:|
| *S3 Bucket Configuration* |


![dynamodb-table](docs/screenshots/2.png) |
|:--:|
| *DynamoDB Table* |









## Requirements

In order to run this project you will need: 

- [Ubuntu 18.04][ubuntu] - Linux operating system
- [Terraform][terraform] - Write, Plan, and Create Infrastructure as Code




## Usage

```
make init install
make terraform/plan STAGE=development
make terraform/apply STAGE=development
```
Outputs:
```
dynamodb_table_arn = arn:aws:dynamodb:ap-southeast-2:096373988534:table/sk-dev-terraform-state-lock
dynamodb_table_id = sk-dev-terraform-state-lock
dynamodb_table_name = sk-dev-terraform-state-lock
s3_bucket_arn = arn:aws:s3:::sk-dev-terraform-state
s3_bucket_domain_name = sk-dev-terraform-state.s3.amazonaws.com
s3_bucket_id = sk-dev-terraform-state
terraform_backend_config = terraform {
  required_version = ">= 0.11.3"

  backend "s3" {
    region         = "ap-southeast-2"
    bucket         = "sk-dev-terraform-state"
    key            = "terraform.tfstate"
    dynamodb_table = "sk-dev-terraform-state-lock"
    profile        = ""
    role_arn       = ""
    encrypt        = "true"
  }
}
```







## Makefile targets

```Available targets:

  aws-nuke/install                   	Install aws-nuke
  base                               	Runs base playbook
  clean                              	Clean roots
  docker/install                     	Install docker
  gomplate/install                   	Install gomplate
  google-chrome/install              	Install google-chrome
  help/all                           	Display help for all targets
  help/all/plain                     	Display help for all targets
  help                               	Help screen
  help/short                         	This help short screen
  install                            	Install the project requirements
  java/install                       	Install java
  openvpn/install                    	Install openvpn
  packer/install                     	Install packer
  packer/version                     	Prints the packer version
  pip/install                        	Install pip
  readme                             	Alias for readme/build
  readme/build                       	Create README.md by building it from README.yaml
  readme/install                     	Install README
  security/install                   	Install security
  spotify/install                    	Install spotify
  terraform/apply                    	Builds or changes infrastructure
  terraform/clean                    	Cleans Terraform vendor from Maker
  terraform/console                  	Interactive console for Terraform interpolations
  terraform/destroy                  	Destroy Terraform-managed infrastructure, removes .terraform and local state files
  terraform/fmt                      	Rewrites config files to canonical format
  terraform/get                      	Download and install modules for the configuration
  terraform/graph                    	Create a visual graph of Terraform resources
  terraform/init-backend             	Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
  terraform/init                     	Initialize a Terraform working directory
  terraform/install                  	Install terraform
  terraform/output                   	Read an output from a state file
  terraform/plan                     	Generate and show an execution plan
  terraform/providers                	Prints a tree of the providers used in the configuration
  terraform/push                     	Upload this Terraform module to Atlas to run
  terraform/refresh                  	Update local state file against real resources
  terraform/show                     	Inspect Terraform state or plan
  terraform/taint                    	Manually mark a resource for recreation
  terraform/untaint                  	Manually unmark a resource as tainted
  terraform/validate                 	Validates the Terraform files
  terraform/version                  	Prints the Terraform version
  terraform/workspace                	Select workspace
  update                             	Updates roots
  vagrant/destroy                    	Stops and deletes all traces of the vagrant machine
  vagrant/install                    	Install vagrant
  vagrant/recreate                   	Destroy and creates the vagrant environment
  vagrant/update/boxes               	Updates all Vagrant boxes
  vagrant/up                         	Starts and provisions the vagrant environment
  version                            	Displays versions of many vendors installed
  virtualbox/install                 	Install virtualbox
  vlc/install                        	Install vlc
```








## References

For additional context, refer to some of these links. 

- [AWS S3](https://aws.amazon.com/s3/) - Amazon Simple Storage Service (Amazon S3) is an object storage service that offers industry-leading scalability, data availability, security, and performance.
- [AWS DynamoDB](https://aws.amazon.com/dynamodb/) - Amazon DynamoDB is a key-value and document database that delivers single-digit millisecond performance at any scale. It's a fully managed, multiregion, multimaster database with built-in security, backup and restore, and in-memory caching for internet-scale applications.




## Resources

Resources used to create this project: 

- [Photo](https://unsplash.com/) - Unsplash.com
- [Gitignore.io](https://gitignore.io) - Defining the `.gitignore`
- [LunaPic](https://www341.lunapic.com/editor/) - Image editor (used to create the avatar)





## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**











## Contributors

Thank you so much for making this project possible: 

- [Valter Silva](https://gitlab.com/valter-silva)



## Copyright

Copyright © 2019-2019 [SkaleSys][company]





[logo]: docs/logo.jpeg


[company]: https://skalesys.com
[contact]: https://skalesys.com/contact
[services]: https://skalesys.com/services
[industries]: https://skalesys.com/industries
[training]: https://skalesys.com/training
[insights]: https://skalesys.com/insights
[about]: https://skalesys.com/about
[join]: https://skalesys.com/Join-Our-Team

[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/




[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/






[branches]: https://gitlab.com/skalesys/terraform-backend/branches
[commits]: https://gitlab.com/skalesys/terraform-backend/commits
[tags]: https://gitlab.com/skalesys/terraform-backend/tags
[contributors]: https://gitlab.com/skalesys/terraform-backend/graphs
[graph]: https://gitlab.com/skalesys/terraform-backend/network
[charts]: https://gitlab.com/skalesys/terraform-backend/charts


